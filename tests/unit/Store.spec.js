import mutations from '@/store/mutations';
import getters from '@/store/getters';
import {
  ADD_TASK,
  REMOVE_TASK,
  TOGGLE_ITEM,
} from '@/store/mutationType';


test('ADD_TASK mutation will add the new task to state', () => {
  const state = {
    tasks: [{
      id: 0,
      task: 'test1',
      completed: false,
    }],
  };
  const newTask = {
    id: 1,
    task: 'test2',
    completed: false,
  };
  mutations[ADD_TASK](state, newTask);
  expect(state.tasks[0]).toBe(newTask);
});

test('REMOVE_TASK mutation will remove the selected task from state', () => {
  const removeTask = {
    id: 1,
    task: 'test2',
    completed: false,
  };
  const remainTask = {
    id: 0,
    task: 'test1',
    completed: false,
  };
  const state = {
    tasks: [remainTask, removeTask],
  };
  mutations[REMOVE_TASK](state, removeTask);
  expect(state.tasks.length).toBe(1);
  expect(state.tasks[0]).toBe(remainTask);
});

test('TOGGLE_ITEM mutation will toggle completed status', () => {
  const remainTask = {
    id: 0,
    task: 'test1',
    completed: false,
  };
  const toggleTask = {
    id: 1,
    task: 'test2',
    completed: false,
  };
  const state = {
    tasks: [remainTask, toggleTask],
  };
  mutations[TOGGLE_ITEM](state, toggleTask);
  expect(state.tasks.length).toBe(2);
  expect(state.tasks[0].completed).toBe(!toggleTask.completed);
  expect(state.tasks[1].completed).toBe(remainTask.completed);
});

test('Getters will able to get correct state', () => {
  const task1 = {
    id: 0,
    task: 'test1',
    completed: false,
  };
  const task2 = {
    id: 1,
    task: 'test2',
    completed: true,
  };

  const state = {
    tasks: [task1, task2],
  };
  expect(getters.getTaskById(state)(0)).toBe(task1);
  expect(getters.getTaskById(state)(1)).toBe(task2);
  expect(getters.pendingTasks(state).length).toBe(1);
  expect(getters.pendingTasks(state)[0]).toBe(task1);
  expect(getters.completedTasks(state).length).toBe(1);
  expect(getters.completedTasks(state)[0]).toBe(task2);
});

