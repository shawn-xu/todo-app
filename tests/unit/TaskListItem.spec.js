import { mount, createLocalVue } from '@vue/test-utils';
import Vuex from 'vuex';
import TaskListItem from '@/components/TaskListItem.vue';

const localVue = createLocalVue();
localVue.use(Vuex);

describe('TaskListItem.vue', () => {
  let actions;
  let store;
  let getters;
  let propsData;

  beforeEach(() => {
    getters = {
      getTaskById() {
        return () => ({
          id: 0,
          task: 'conduct test',
          completed: true,
        });
      },
    };

    actions = {
      removeTask: jest.fn(),
      toggleItem: jest.fn(),
    };

    store = new Vuex.Store({
      state: {
        taskList: [{
          id: 0,
          task: 'conduct test',
          completed: true,
        }],
      },
      actions,
      getters,
    });

    propsData = {
      todo: {
        id: 0,
        task: 'conduct test',
        completed: true,
      },
    };
  });

  it('matches snapshot', () => {
    const wrapper = mount(TaskListItem, {
      propsData, store, localVue,
    });
    expect(wrapper.html()).toMatchSnapshot();
  });

  // to confirm actions successfully issued
  it('issue toggleitem action when click on checkbox', () => {
    const wrapper = mount(TaskListItem, {
      propsData, store, localVue,
    });
    const checkbox = wrapper.find('.el-checkbox');
    checkbox.trigger('click');
    expect(actions.toggleItem).toHaveBeenCalled();
  });

  it('issue removeTask action when click on delete button', () => {
    const wrapper = mount(TaskListItem, {
      propsData, store, localVue,
    });
    const button = wrapper.find('.el-button');
    button.trigger('click');
    expect(actions.removeTask).toHaveBeenCalled();
  });

  // to confirm the three key elements have been rendered if snapshots fail
  it('renders the checkbox element', () => {
    const wrapper = mount(TaskListItem, {
      propsData, store, localVue,
    });
    expect(wrapper.html()).toContain('<div class="completed todo-task"> conduct test</div>');
    expect(wrapper.html()).toContain('<input type="checkbox" aria-hidden="true" class="el-checkbox__original" value="">');
    expect(wrapper.html()).toContain('<button type="button" class="el-button delete-button el-button--danger el-button--mini is-circle"><!----><i class="el-icon-delete"></i><!----></button>');
  });
});
