import firebase from 'firebase/app';
import 'firebase/firestore';

firebase.initializeApp({
  authDomain: 'todo-acronis.firebaseapp.com',
  projectId: 'todo-acronis',
});

const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);

// Initialize Cloud Firestore through Firebase
export const db = firebase.firestore();
export const arrayUnion = firebase.firestore.FieldValue.arrayUnion;
export const arrayRemove = firebase.firestore.FieldValue.arrayRemove;


// db.collection("state").doc("state").get().then((doc) => {
//   })
// db.collection("state").doc("state").set(initial)
