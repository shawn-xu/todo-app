import _clone from 'lodash/clone';
import {
  db,
  arrayUnion,
  arrayRemove,
} from './firestore-connector';


export const getAllTasksAPI = () => db.collection('state').doc('state').get()
  .then(doc => doc.data())
  .catch((error) => {
    console.error('Error retrive the document: ', error);
  });

export const addTaskAPI = task => db.collection('state').doc('state').update({
  tasks: arrayUnion(task),
})
  .catch((error) => {
    console.error('Error update the document: ', error);
  });

export const removeTaskAPI = task => db.collection('state').doc('state').update({
  tasks: arrayRemove(task),
})
  .catch((error) => {
    console.error('Error update the document: ', error);
  });

export const toggleTaskAPI = (task) => {
  const taskAdd = _clone(task);
  taskAdd.completed = !taskAdd.completed;

  const promiseAdd = db.collection('state').doc('state').update({
    tasks: arrayUnion(taskAdd),
  })
    .catch((error) => {
      console.error('Error update the document: ', error);
    });
  const promiseRemove = db.collection('state').doc('state').update({
    tasks: arrayRemove(task),
  })
    .catch((error) => {
      console.error('Error update the document: ', error);
    });

  return Promise.all([promiseAdd, promiseRemove])
    .catch((error) => {
      console.error('Error update the document: ', error);
    });
};
