const axios = require('axios');

const getPageViewCounter = () => {
  const param = {
    method: 'get',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
  };
  return axios('http://45.77.9.165:3000/', param)
    .then(response => response.data.count);
};

export default getPageViewCounter;
