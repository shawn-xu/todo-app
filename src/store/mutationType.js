export const ADD_TASK = 'ADD_TASK';
export const REMOVE_TASK = 'REMOVE_TASK';
export const TOGGLE_ITEM = 'TOGGLE_ITEM';
export const RELOAD_ALERT = 'RELOAD_ALERT';

export default {
  ADD_TASK,
  REMOVE_TASK,
  TOGGLE_ITEM,
  RELOAD_ALERT,
};
