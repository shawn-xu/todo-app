import _union from 'lodash/union';
import _clone from 'lodash/clone';

import {
  ADD_TASK,
  REMOVE_TASK,
  TOGGLE_ITEM,
  RELOAD_ALERT,
} from './mutationType';

export default {
  [ADD_TASK](state, task) {
    state.tasks = _union([task], state.tasks);
  },

  [REMOVE_TASK](state, task) {
    state.tasks = state.tasks.filter(x => x.id !== task.id);
  },

  [TOGGLE_ITEM](state, task) {
    const taskAdd = _clone(task);
    taskAdd.completed = !taskAdd.completed;
    state.tasks = state.tasks.filter(x => x.id !== task.id);
    state.tasks = _union([taskAdd], state.tasks);
  },

  [RELOAD_ALERT](state) {
    state.reload = true;
  },
};
