import { timeout } from 'promise-timeout';
import {
  getAllTasksAPI,
  addTaskAPI,
  removeTaskAPI,
  toggleTaskAPI,
} from '@/service/firestore-api';
import * as types from './mutationType';

export const allTask = ({ commit }) => getAllTasksAPI()
  .then((tasks) => {
    tasks.tasks.forEach((task) => {
      commit(types.ADD_TASK, task);
    });
  });

export const addTask = ({ commit }, task) => {
  commit(types.ADD_TASK, task.taskItem);
  timeout(addTaskAPI(task.taskItem), 2500)
    .catch(() => {
      commit(types.RELOAD_ALERT);
    });
};

export const removeTask = ({ commit }, task) => {
  commit(types.REMOVE_TASK, task.taskItem);
  timeout(removeTaskAPI(task.taskItem), 2500)
    .catch(() => {
      commit(types.RELOAD_ALERT);
    });
};


export const toggleItem = ({ commit }, task) => {
  commit(types.TOGGLE_ITEM, task);
  timeout(toggleTaskAPI(task), 2500)
    .catch(() => {
      commit(types.RELOAD_ALERT);
    });
};


export default {
  allTask,
  addTask,
  removeTask,
  toggleItem,
};
