
const getTaskById = state => id => state.tasks.find(x => x.id === id);
const pendingTasks = state => state.tasks.filter(task => !task.completed);
const completedTasks = state => state.tasks.filter(task => task.completed);

export default {
  getTaskById,
  pendingTasks,
  completedTasks,
};
