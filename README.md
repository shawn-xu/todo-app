# Todo-app

## Development
The todo project is developed using `Vue.js + Vuex + Vue Router` as basic structure. 
Element UI components are used for the app development.
A live demo of the app can be accessed at [Live Demo](http://todo-app-shawn.herokuapp.com/)

### Compiles and hot-reloads for development
```
$ npm start
```

## Backend
The google Firestore is used as backend to storage the todo tasks. 
As the Firestore SDK uses [gRPC](https://grpc.io/) to communicate with the server, the Axios is demonstrate by a "Page View Counter" in the "About" page through a Express server connecting to Firestore.
The Express server scripts can be found at /server folder. It's already deployed to http://45.77.9.165:3000/ for this project.

## Unit Test
Vue-test-utils with Jest is used for the Unit Testing. 
To demonstrate, testing has been written for component `TaskLiteItem` and vuex `Store`.
To run unit test:
``` bash
$ npm test
```

## Directory Structure
##### main.js
This file will load the to-do application. It is the entry point of the app.

##### src/App.vue
The main Vue file. This file will load the page inside the `router-view`-component.

##### components/
Vue components built for this projects.

##### pages/
Vue components that function as pages. The pages are used for router.

##### Service/
Firestore and Express server connectors to perform AJAX-requests. Also include the promise instance exports to use by other components.

##### router.js/
VueRouter routes.

##### store/
Vuex store which includes the Vuex elements.
 - actions
 - getters
 - mutations
 - mutation-types
 - state

#### server/
This project comes with a mock api server implemented with [Express](https://expressjs.com/) . The server scripts is located in this folder.
