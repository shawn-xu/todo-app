let {getCount,setCount} = require('../firestore-api');
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(_, res) {
  getCount().then(data=>{
    let newCount ={
      count:data.count+1,
    }
    setCount().set(newCount)
    .then(()=>{
      res.send(data);
    });
  });
  
});

module.exports = router;
