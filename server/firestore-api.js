const firebase = require("firebase");
require("firebase/firestore");

firebase.initializeApp({
  authDomain: 'todo-acronis.firebaseapp.com',
  projectId: 'todo-acronis',
});

const firestore = firebase.firestore();
const settings = { timestampsInSnapshots: true };
firestore.settings(settings);
const db = firebase.firestore();

const getCount = () => db.collection('state').doc('counter').get()
  .then(doc => doc.data())
  .catch((error) => {
    console.error('Error retrive the document: ', error);
  });

const setCount = () => db.collection('state').doc('counter');

exports.getCount = getCount;
exports.setCount = setCount;